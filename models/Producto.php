<?php

namespace App\Models;

class Producto{
    public $id;
    public $nombre;
    public $descripcion;
    public $precio;
    public $cantidad;
    public $imagen;
    public $db;

    public function __construct($enviroment){
        $this->db = $enviroment->db;
    }

    public function getAll(){
        return $this->db->table('producto')->get();
    }

    public function get($id){
        $producto = $this->db->table('producto')
                            ->where('id',$id)->first();
        return $producto;
    }

    public function sell($arreglo){
        $id = $arreglo["id"];
        $cantidad = $arreglo["cantidad"];
        $venta = $this->db->table('venta')->insert(["producto_id"=>$id, "cantidad"=>$cantidad]);
        return $venta;
    }

    
}